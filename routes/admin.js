var express = require("express");
var router = express.Router();
var mysql=require("mysql");
var md5 = require("md5");
var connection = mysql.createConnection({
  host : "localhost",
  user : "prog_web",
  password : "prog_web",
  database : "prog_web"
  });

 connection.connect();

 // middleware function to check for logged-in users
var sessionChecker = (req, res, next) => {
    req.session.user == "admin" ? next() : res.redirect('/');
};

// Admin - Home
router.get('/',sessionChecker, function(req, res, next) {
    res.redirect("/kasir");
});

// Admin - Employees
router.get("/employees", sessionChecker, function(req, res, next){
    var id = [];
    var name = [];
    var username = [];
    var role = [];

    connection.query("SELECT id,name,username,role FROM users",
      function(err, rows, fields) {
          if(err) res.redirect('/');
          for(i=0;i<rows.length;i++)
          {
              id.push(rows[i].id);
              name.push(rows[i].name);
              username.push(rows[i].username);
              role.push(rows[i].role);
          }
          employees = {
              id,
              name,
              username,
              role
          };
          res.render("employees", {
            session:req.session,
            len: rows.length,
            employees
        });
    });
});

router.get("/employees/add",sessionChecker,function(req,res,next){
    res.render("employee_add",{
        title:"Employee",
        session:req.session
    });
});

router.post("/employees/add",sessionChecker,function(req,res,next){
    if(req.body.name && req.body.username && req.body.user_pw && req.body.role)
    {
      var query = "INSERT INTO users (name,username,user_pw,role) VALUES (?,?,?,?)";
      var employees,employeeNew;
      connection.query(query,[
          req.body.name,
          req.body.username,
          md5(req.body.user_pw),
          req.body.role
        ],function(err){
            if(err) res.redirect('/');
            employeeNew={
              name: req.body.name,
              username: req.body.username, 
              role: req.body.role
            }   
        });
        connection.query("SELECT id,name,username,role FROM users",
        function(err, rows, fields) {
          if(err) res.redirect('/');
          var id = [];
          var name = [];
          var username = [];
          var role = [];
          for(i=0;i<rows.length;i++)
          {
              id.push(rows[i].id);
              name.push(rows[i].name);
              username.push(rows[i].username);
              role.push(rows[i].role);
          }
          employees = {
            id,
            name,
            username,
            role
          }; 
          res.render("employees",{
            title:"Employee",
            session:req.session,
            employeeNew,
            employees,
            len: rows.length,
            msg: "Add employee success"
        });
      });
    } else {
      res.render("employees",{
          title:"Employee",
          session:req.session,
          msg: "Add employee failed"
        });
    }
});

router.post("/employees/delete",sessionChecker,function(req,res,next){
    if(req.body.username && req.body.username != "admin"){
        // TODO : show pop up "are you sure"
        var query = "DELETE FROM users WHERE username = ?";
        connection.query(query,[req.body.username],function(err){
            if(err) res.redirect('/');

            // TODO : add notif
            res.redirect("/admin/employees");
        });
    } else {
        res.redirect("/admin/employees");
    }
});


// Admin - Products
router.get("/products",sessionChecker,function(req,res,next){
    var productCode = [];
    var productName = [];
    var sellPrice = [];
    var buyPrice = [];
    var stock = [];
    var query,products;
    if(req.query.searchProduct){
        query = `SELECT * FROM product WHERE productName LIKE '%${req.query.searchProduct}%' ORDER BY productName`;
    } else {
        query = "SELECT * FROM product";
    }
    connection.query(query,function(err,rows,fields){
        if(err) res.redirect('/');

        for(i=0;i<rows.length;i++){
            productCode.push(rows[i].productCode);
            productName.push(rows[i].productName);
            sellPrice.push(parseInt(rows[i].sellPrice));
            buyPrice.push(parseInt(rows[i].buyPrice));
            stock.push(parseInt(rows[i].stock));
        }
        products = {
            productCode,
            productName,
            sellPrice,
            buyPrice,
            stock
        };
        res.render("products", {
            title: "Product",
            session:req.session,
            products,
            len:rows.length
        });
    });
});

router.get("/products/insert",sessionChecker,function(req,res,next){
    res.render("insert_interface",{
        title:"Product",
        session:req.session
    });
});

router.post("/products/insert",sessionChecker,function(req,res,next){
  if(
      req.body.productCode &&
      req.body.productName && 
      parseInt(req.body.sellPrice) &&
      parseInt(req.body.buyPrice) &&
      parseInt(req.body.stock) &&
      parseInt(req.body.sellPrice)>0 &&
      parseInt(req.body.buyPrice)>0 &&
      parseInt(req.body.sellPrice)>parseInt(req.body.buyPrice) &&
      parseInt(req.body.stock) > 0 )
    {
        var query = "INSERT INTO product VALUES (?,?,?,?,?)";
        var products,productNew;
        connection.query(query,[
            req.body.productCode,
            req.body.productName,
            parseInt(req.body.sellPrice),
            parseInt(req.body.buyPrice),
            parseInt(req.body.stock)
        ], function(err){
            if(err) res.redirect('/');
            productNew={
              productCode: req.body.productCode,
              productName: req.body.productName,
              sellPrice: parseInt(req.body.sellPrice),
              buyPrice: parseInt(req.body.buyPrice),
              stock: parseInt(req.body.stock)
            }
        });
        query = "SELECT * FROM product ORDER BY productName";
        connection.query(query,function(err,rows,fields){
            if(err) res.redirect('/');
            var productCode = [];
            var productName = [];
            var sellPrice = [];
            var buyPrice = [];
            var stock = [];
            for(i=0;i<rows.length;i++){
                productCode.push(rows[i].productCode);
                productName.push(rows[i].productName);
                sellPrice.push(parseInt(rows[i].sellPrice));
                buyPrice.push(parseInt(rows[i].buyPrice));
                stock.push(parseInt(rows[i].stock));
            }
            products = {
                productCode,
                productName,
                sellPrice,
                buyPrice,
                stock
            };
        
            res.render("products",{
                title:"Product",
                productNew,
                products,
                session:req.session,
                len:rows.length,
                msg: "Insert product success"
            });
        });
    } else {
      res.render("products",{
          title: "Product",
          msg: "Insert product failed",
          session:req.session
      });
    }
});

router.post("/products/add",sessionChecker,function(req,res,next){
  if(req.body.productKey){
      var query = "UPDATE product SET stock = stock + ? WHERE productCode = ?";
      connection.query(query,[parseInt(req.body.stockAdded),req.body.productKey],function(err){
          if(err) res.redirect('/');
          else {
            var productCode = [];
            var productName = [];
            var sellPrice = [];
            var buyPrice = [];
            var stock = [];
            connection.query("SELECT * FROM product ORDER BY productName",function(err,rows,fields){
                if(err) res.redirect('/');
        
                for(i=0;i<rows.length;i++)
                {
                    productCode.push(rows[i].productCode);
                    productName.push(rows[i].productName);
                    sellPrice.push(parseInt(rows[i].sellPrice));
                    buyPrice.push(parseInt(rows[i].buyPrice));
                    stock.push(parseInt(rows[i].stock));
                }
                products = {
                    productCode,
                    productName,
                    sellPrice,
                    buyPrice,
                    stock
                };
                res.render("products", {
                    title:"Product",
                    session:req.session,
                    products,
                    len:rows.length,
                    msg:'Success add stock'
                });
            });
        }
    });  
  } else{
      res.redirect("/admin/products");
  }
});

router.post("/products/delete",sessionChecker,function(req,res,next){
    if(req.body.productKey){
        // TODO : munculin form "are you sure"
        var query = "DELETE FROM product WHERE productCode = ?";
        connection.query(query,[req.body.productKey],function(err){
            if(err) res.redirect('/');
            else{
                res.redirect("/admin/products");
            }
        });
    } else{
        res.redirect("/products");
    }
});

module.exports = router;