var express = require('express');
var router = express.Router();
var mysql=require('mysql');
var connection = mysql.createConnection({
  host : 'localhost',
  user : 'prog_web',
  password : 'prog_web',
  database : 'prog_web'
  });

 connection.connect();

// middleware function to check for logged-in users
var sessionChecker = (req, res, next) => {
  req.session.user ? next(): res.redirect('/login');
};

/* GET home page.*/
router.get('/',sessionChecker, function(req, res, next) {
    var orderCode = [];
    var payTime = [];
    var cashierName = [];
    var total = [];

    connection.query(`SELECT orderCode, pay_time, cashierUName, SUM(priceEach*qty) AS total FROM payment      
      JOIN orders USING (orderCode)          
      WHERE DAY(pay_time) = DAY(NOW())     
      GROUP BY orderCode`,
      function(err, rows, fields) {
          if(err) res.redirect('/');
          for(i=0;i<rows.length;i++)
          {
              orderCode.push(rows[i].orderCode);
              payTime.push(rows[i].pay_time);
              cashierName.push(rows[i].cashierUName);
              total.push(rows[i].total);
          }
          res.render('riwayat', {session:req.session,
            len: rows.length, 
            orderCode: orderCode,
            payTime: payTime, 
            cashierName: cashierName, 
            total: total
        });
    });
});

router.post('/detail', sessionChecker, function(req, res, next){
    var orderCode = parseInt(req.body.key);

    var productCode = [];
    var productName = [];
    var qty = [];
    var priceEach = [];
    var subTotal = [];
    var total = 0;

    var query = `SELECT productCode,productName,qty,priceEach,(qty*priceEach) AS subTotal FROM payment 
    JOIN orders USING (orderCode) WHERE orderCode = ${orderCode}`;
    
    connection.query(query,
      function(err, rows, fields) {
          if(err) res.redirect('/');
          for(i=0;i<rows.length;i++)
          {
              productCode.push(rows[i].productCode);
              productName.push(rows[i].productName);
              qty.push(parseInt(rows[i].qty));
              priceEach.push(parseInt(rows[i].priceEach));
              subTotal.push(parseInt(rows[i].subTotal));
              total += rows[i].subTotal;
          }
          res.render('riwayat_detail', {session:req.session,
            
            len: rows.length, 
            productCode,
            productName,
            qty,
            priceEach,
            subTotal,
            total
        });
    });
});

module.exports = router;