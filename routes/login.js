var express = require('express');
var router = express.Router();
var mysql=require('mysql');
var md5 = require('md5');
var connection = mysql.createConnection({
  host : 'localhost',
  user : 'prog_web',
  password : 'prog_web',
  database : 'prog_web'
  });

 connection.connect();

 // middleware function to check for logged-in users
var sessionChecker = (req, res, next) => {
    if (req.session.user) {
        res.redirect('../');
    } else {
        next();
    }    
};

 router.get('/',sessionChecker,function(req,res,next){
     res.render('login');
 });
 router.post('/',sessionChecker,function(req,res,next){
     if(typeof(req.body.username)!='undefined' && typeof(req.body.password)!='undefined'){
        connection.query('SELECT * FROM users WHERE username = ? AND user_pw = ?',[req.body.username,md5(req.body.password)],
            function(err,rows,field){
                if(rows.length == 1){

                    req.session.user = rows[0].username;
                    res.redirect('../');
                } else {
                    res.render('login',{msg: "Username / password salah"});
                }
            });
     } else{

     }
 });


 module.exports = router;