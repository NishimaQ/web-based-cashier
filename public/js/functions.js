function addToCart(code,name,price,qtyID,cart){
  qty = document.getElementById(qtyID).value;
  post('/kasir',{pCode:code,pName:name,price:price,qty:qty,cart:JSON.stringify(cart),isDeleted:false},"POST")
}

function post(path, params, method) {
  method = method || "post"; 

  var form = document.createElement("form");
  form.setAttribute("method", method);
  form.setAttribute("action", path);
  
  for(var key in params) {
      if(params.hasOwnProperty(key)) {
          var hiddenField = document.createElement("input");
          hiddenField.setAttribute("type", "hidden");
          hiddenField.setAttribute("name", key);
          hiddenField.setAttribute("value", params[key]);
  
          form.appendChild(hiddenField);
      }
  }
  document.body.appendChild(form);
  form.submit();
}

function myFunc(cart){
  var argument = {};
  argument.searchVal = document.getElementById('search-box').value;
  argument.cart = cart;
  $.post('/kasir/search',argument, function(responseText) {});
}

function deleteKey(cart,key){
  cart = JSON.parse(cart);
  delete cart[key];
  qty=1;
  post('/kasir',{cart: JSON.stringify(cart), isDeleted: true, qty},'POST');
}